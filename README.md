# Smart mirror

## Table of content

- [Intro](#intro)
- [Setup](#setup)
- [Main informations big icons (weather, temperature & wind)](#main-informations-big-icons-weather-temperature--wind)
- [Preview](#preview)
- [Photo of a beta](#photo-of-a-beta)
- [Next steps / TODO](#next-steps--todo)

## Intro

Openweathermap one call API is limited to 1000 free calls per day.

We use 2 update frequencies, for a total of 849 calls per 24h:

- Every 70 seconds during the "day", from 7h to 22h, for a total of 772 calls.
- Every 7 minutes during the "night", from 22h to 7h, for a total of 77 calls.

Openweathermap server side update seems to occur every 60 to 70 seconds, therefore it's not recommended to set a frequency higher than 1/70s, otherwise you migh do an useless call, and double the time to get updated datas.

## Setup

- Open an account on https://openweathermap.org/
- Replace `YOUR_API_KEY` by your newly created account's API key in [js/index.js](https://gitlab.com/baptistecs/smart-mirror/-/blob/master/js/index.js#L59)
- Open index.html in your browser
- Enter full screen in your browser

That's it!

## Main informations big icons (weather, temperature & wind)

You can enable the big icons by setting the `withMainInfo` to `true` when calling the `oneCallToCurrentWeatherHTMLFlex` function [there](https://gitlab.com/baptistecs/smart-mirror/-/blob/master/js/index.js#L712)

Doing so, it will get out of a 800 x 1280 screen, unless:

- We reduce the line height and the font size but it will become more difficult to read.
- We remove the hourly forcast or the daily forecast sections.

It will look like this :
![Main informations big icons](img/main-info-big-icons.png 'Main informations big icons')

## Preview

It should look like this:
![Alt text](img/preview.png 'Smart mirror preview')

## Photo of a beta

![Alt text](img/photo.jpg 'Smart mirror beta photo')

## Next steps / TODO

- Add air quality index (current & forecast) [API doc](https://openweathermap.org/api/air-pollution)
- Add air composition details (current & forecast) [API doc](https://openweathermap.org/api/air-pollution)
