﻿// https://openweathermap.org/weather-conditions
// https://openweathermap.org/api/one-call-api
// https://openweathermap.org/current
// https://openweathermap.org/city/2673730
// 1000 free calls per day using one call API
const windCompass = (wind_deg = 0, width = 0, height = 0) => `
<style>
.wind-arrow {
  transform-origin: center center;
  transform: rotate(${wind_deg + 90}deg);
}
</style>
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg ${width !== 0 ? `width="${width}"` : ''}${height !== 0 ? `height="${height}"` : ''}
  xmlns="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  viewBox="0 0 180 180"
>
  <defs>
    <style type="text/css">
      .stroke,
      .wind-arrow {
        stroke: white;
        stroke-width: 1;
      }
      .wind-arrow {
        stroke: black;
        fill: white;
        stroke-width: 0.5;
      }
      .empty {
        fill: black;
      }
      .fill {
        fill: white;
      }
    </style>
  </defs>
  <use xlink:href="#cross" transform="matrix(.26482 -.63932 .63932 .26482 10.487 124.19)" height="1052.3622" width="744.09448" y="0" x="0" />
  <use xlink:href="#cross" transform="matrix(.63932 -.26482 .26482 .63932 9.4972 56.448)" height="1052.3622" width="744.09448" y="0" x="0" />
  <use xlink:href="#cross" transform="matrix(.61971 .61971 -.61971 .61971 90.131 -22.976)" height="1052.3622" width="744.09448" y="0" x="0" />
  <g id="cross" transform="matrix(.38217 0 0 .38217 37.107 -156.88)">
    <g id="cross-branch" transform="matrix(.74119 0 0 1 37.017 0)">
      <path class="empty stroke" d="m142 640.38l1 1 38.06-38.07-38.03-141.25-1.03 3.85v174.47z" />
      <path class="fill stroke" d="m142 640.38v-174.47l-37 137.4 37 37.07z" />
    </g>
    <use xlink:href="#cross-branch" transform="matrix(0 1 -1 0 786.75 500.69)" height="1052.3622" width="744.09448" y="0" x="0" />
    <use xlink:href="#cross-branch" transform="matrix(-1 0 0 -1 286.06 1287.4)" height="1052.3622" width="744.09448" y="0" x="0" />
    <use xlink:href="#cross-branch" transform="matrix(0 -1 1 0 -500.69 786.75)" height="1052.3622" width="744.09448" y="0" x="0" />
  </g>
  <path id="north" class="fill" d="m96.179 17.638h-1.651l-5.732-9.0495v9.0495h-1.437v-11.144l1.666 0.0003 5.717 9.0347v-9.0347h1.437v11.144" />
  <path id="south" class="fill" d="m96.103 170.01c0 1.08-0.459 1.95-1.376 2.61-0.826 0.59-1.799 0.89-2.92 0.89-1.253 0-2.283-0.35-3.088-1.04-0.856-0.74-1.284-1.73-1.284-2.97h1.437c0 0.86 0.255 1.53 0.764 2.03 0.51 0.5 1.188 0.75 2.034 0.75 0.693 0 1.324-0.16 1.895-0.49 0.683-0.39 1.024-0.91 1.024-1.56 0-0.79-0.586-1.37-1.758-1.74-1.09-0.3-2.175-0.61-3.256-0.93-1.182-0.52-1.773-1.35-1.773-2.49 0-1.04 0.403-1.87 1.208-2.48 0.733-0.56 1.635-0.84 2.705-0.84 1.132 0 2.069 0.31 2.813 0.92 0.815 0.66 1.223 1.55 1.223 2.66h-1.437c0-0.74-0.234-1.32-0.703-1.75-0.469-0.42-1.075-0.64-1.819-0.64-0.632 0-1.192 0.16-1.682 0.46-0.581 0.37-0.871 0.85-0.871 1.45 0 0.76 0.591 1.31 1.773 1.65 1.101 0.31 2.202 0.62 3.302 0.92 1.193 0.53 1.789 1.39 1.789 2.59" />
  <path id="west" class="fill" d="m20.333 83.554l-2.95 11.144h-1.498l-2.553-9.202-2.446 9.202h-1.5439l-2.9197-11.144h1.4981l2.2775 8.912 2.354-8.912h1.59l2.431 8.851 2.323-8.851h1.437" />
  <path id="east" class="fill" d="m173.58 94.698h-8.1v-11.144h7.98v1.361h-6.46v3.348h5.94v1.299h-5.94v3.761h6.58v1.375" />
  <path class="wind-arrow" d="M19 89 57.78 96.7 54 91.8H114.48L132.82 96.7H159L140.52 91.8V86.2L159 81.3H132.82L114.48 86.2H54L57.78 81.3Z" />
</svg>`;

const openweathermap = {
  appid: 'YOUR_API_KEY',
  weatherUrl: (appid) => `https://api.openweathermap.org/data/2.5/weather?appid=${appid}`,
  weatherGroupUrl: (appid) => `https://api.openweathermap.org/data/2.5/group?appid=${appid}`,
  onecallUrl: (appid) => `https://api.openweathermap.org/data/2.5/onecall?appid=${appid}`,
  api: {
    mode: {
      json: 'json',
      html: 'html',
      xml: 'xml',
    },
    units: {
      standard: 'standard', // Kelvin
      metric: 'metric', // Celsius
      imperial: 'imperial', // Fahrenheit
    },
    lang: {
      af: 'af', // Afrikaans
      al: 'al', // Albanian
      ar: 'ar', // Arabic
      az: 'az', // Azerbaijani
      bg: 'bg', // Bulgarian
      ca: 'ca', // Catalan
      cz: 'cz', // Czech
      da: 'da', // Danish
      de: 'de', // German
      el: 'el', // Greek
      en: 'en', // English
      eu: 'eu', // Basque
      fa: 'fa', // Persian (Farsi)
      fi: 'fi', // Finnish
      fr: 'fr', // French
      gl: 'gl', // Galician
      he: 'he', // Hebrew
      hi: 'hi', // Hindi
      hr: 'hr', // Croatian
      hu: 'hu', // Hungarian
      id: 'id', // Indonesian
      it: 'it', // Italian
      ja: 'ja', // Japanese
      kr: 'kr', // Korean
      la: 'la', // Latvian
      lt: 'lt', // Lithuanian
      mk: 'mk', // Macedonian
      no: 'no', // Norwegian
      nl: 'nl', // Dutch
      pl: 'pl', // Polish
      pt: 'pt', // Portuguese
      pt_br: 'pt_br', // Português Brasil
      ro: 'ro', // Romanian
      ru: 'ru', // Russian
      sv: 'sv, se', // Swedish
      sk: 'sk', // Slovak
      sl: 'sl', // Slovenian
      es: 'sp, es', // Spanish
      sr: 'sr', //Serbian
      th: 'th', //Thai
      tr: 'tr', //Turkish
      uk: 'ua, uk', // Ukrainian
      vi: 'vi', // Vietnamese
      zh_cn: 'zh_cn', // Chinese Simplified
      zh_tw: 'zh_tw', // Chinese Traditional
      zu: 'zu', // Zulu
    },
    icons: {
      '01d': 'clear-sky', // day
      '02d': 'few-clouds', // (from 1/8 to 2/8)
      '03d': 'scattered-clouds', // (from 3/8 to 4/8)
      '04d': 'broken-clouds', // (from 5/8 to 7/8) + overcast-clouds (8/8)
      '09d': 'shower-rain',
      '10d': 'rain',
      '11d': 'thunderstorm',
      '13d': 'snow',
      '50d': 'mist',
      '01n': 'clear-sky', // night
      '02n': 'few-clouds',
      '03n': 'scattered-clouds',
      '04n': 'broken-clouds',
      '09n': 'shower-rain',
      '10n': 'rain',
      '11n': 'thunderstorm',
      '13n': 'snow',
      '50n': 'mist',
    },
  },
};
const {
  weatherUrl,
  weatherGroupUrl,
  onecallUrl,
  appid,
  api: {
    mode: { json },
    units: { metric },
    lang: { en },
  },
} = openweathermap;

/**
 * You can exclude a part of the response by using a coma separated list:
 * exclude: current,minutely,hourly,daily,alerts
 **/
const getAllByLonLatWithOneCall = async ({ lon, lat, exclude = '', lang = en, units = metric, mode = json }) => {
  logNumberOfRequest('onecall');
  const url = `${onecallUrl(appid)}&lon=${lon}&lat=${lat}&exclude=${exclude}&units=${units}&lang=${lang}`;
  const response = await fetch(url);
  const { ok, status, statusText } = response;
  const result = await response.json();
  return { ok, status, statusText, result };
};

/**
 * City id list: http://bulk.openweathermap.org/sample/city.list.json.gz
 * Stockholm: 2673730 * Argelliers: 3028337 * Montpellier: 2992166 * Rodez: 2983154
 **/
const getCurrentWeatherByCityIds = async (
  cityIds = '2673730,3028337,2983154',
  lang = en,
  units = metric,
  mode = json,
) => {
  logNumberOfRequest('group');
  const response = await fetch(`${weatherGroupUrl(appid)}&id=${ids}&units=${units}&mode=${mode}&lang=${lang}`);
  const body = await (mode === json ? response.json() : response.text());
  return body;
};

const getCurrentWeatherByCityName = async (city = 'stockholm', lang = en, units = metric, mode = json) => {
  logNumberOfRequest('weather');
  const response = await fetch(`${weatherUrl(appid)}&q=${city}&units=${units}&mode=${mode}&lang=${lang}`);
  const body = await (mode === json ? response.json() : response.text());
  return body;
};

// Clouds: 75%
// Humidity: 93%
// Wind: 6.17 m/s
// Pressure: 994hpa
// 1°C
// Feels like -5°C. Light rain and snow. Moderate breeze
// 0.15mm/h
//  6.2m/s E
// 994hPa
// Humidity:
// 93%
// Dew point:
// 0°C
// Visibility:
// 4.3km

// base: "stations"
// clouds: {all: 75}
// cod: 200
// coord: {lon: 18.0649, lat: 59.3326}
// dt: 1611173656
// id: 2673730
// main: { feels_like: -5.19, humidity: 93, pressure: 994, temp: 1.1, temp_max: 1.67, temp_min: 0.56 }
// name: "Stockholm"
// snow: {1h: 0.15}
// sys: { type: 1, id: 1788, country: "SE", sunrise: 1611127332, sunset: 1611153299 }
// timezone: 3600
// visibility: 4300
// weather: [{id: 615, main: "Snow", description: "light rain and snow", icon: "13n"}]
// wind: {speed: 6.17, deg: 100}

// {
//   "coord": {
//     "lon": -122.08,
//     "lat": 37.39
//   },
//   "weather": [
//     {
//       "id": 800, // Weather condition id
//       "main": "Clear", // Group of weather parameters (Rain, Snow, Extreme etc.)
//       "description": "clear sky", // Weather condition within the group. You can get the output in your language.
//       "icon": "01d" // Weather icon id // https://openweathermap.org/weather-conditions
//     }
//   ],
//   "base": "stations",
//   "main": {
//     "temp": 282.55, // Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
//     "feels_like": 281.86,
//     "temp_min": 280.37,
//     "temp_max": 284.26,
//     "pressure": 1023, // Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
//     "humidity": 100 // %
//   },
//   "visibility": 16093, // meter
//   "wind": {
//     "speed": 1.5, // Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
//     "deg": 350 // Wind direction, degrees (meteorological)
//     "gust": 55, // Wind gust. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour
//   },
//   "clouds": {
//     "all": 1 // Cloudiness, %
//   },
//   "snow": {
//     "1h": 125, // Snow volume for the last 1 hour, mm
//     "3h": 234, // Snow volume for the last 3 hours, mm
//   },
//   "dt": 1560350645, // Time of data calculation, unix, UTC
//   "sys": {
//     "type": 1,
//     "id": 5122,
//     "message": 0.0139,
//     "country": "US",
//     "sunrise": 1560343627, // Sunrise time, unix, UTC
//     "sunset": 1560396563, // Sunset time, unix, UTC
//   },
//   "timezone": -25200, // Shift in seconds from UTC
//   "id": 420006353, // City ID
//   "name": "Mountain View", // City name
//   "cod": 200
//   }

const weatherIdToDescription = (id) => {
  // prettier-ignore
  switch (id) {
    case 200: return 'thunderstorm with light rain'; // 11d // Thunderstorm
    case 201: return 'thunderstorm with rain'; // 11d // Thunderstorm
    case 202: return 'thunderstorm with heavy rain'; // 11d // Thunderstorm
    case 210: return 'light thunderstorm'; // 11d // Thunderstorm
    case 211: return 'thunderstorm'; // 11d // Thunderstorm
    case 212: return 'heavy thunderstorm'; // 11d // Thunderstorm
    case 221: return 'ragged thunderstorm'; // 11d // Thunderstorm
    case 230: return 'thunderstorm with light drizzle'; // 11d // Thunderstorm
    case 231: return 'thunderstorm with drizzle'; // 11d // Thunderstorm
    case 232: return 'thunderstorm with heavy drizzle'; // 11d // Thunderstorm
    case 300: return 'light intensity drizzle'; // 09d // Drizzle
    case 301: return 'drizzle'; // 09d // Drizzle
    case 302: return 'heavy intensity drizzle'; // 09d // Drizzle
    case 310: return 'light intensity drizzle rain'; // 09d // Drizzle
    case 311: return 'drizzle rain'; // 09d // Drizzle
    case 312: return 'heavy intensity drizzle rain'; // 09d // Drizzle
    case 313: return 'shower rain and drizzle'; // 09d // Drizzle
    case 314: return 'heavy shower rain and drizzle'; // 09d // Drizzle
    case 321: return 'shower drizzle'; // 09d // Drizzle
    case 500: return 'light rain'; // 10d // Rain
    case 501: return 'moderate rain'; // 10d // Rain
    case 502: return 'heavy intensity rain'; // 10d // Rain
    case 503: return 'very heavy rain'; // 10d // Rain
    case 504: return 'extreme rain'; // 10d // Rain
    case 511: return 'freezing rain'; // 13d // Rain
    case 520: return 'light intensity shower rain'; // 09d // Rain
    case 521: return 'shower rain'; // 09d // Rain
    case 522: return 'heavy intensity shower rain'; // 09d // Rain
    case 531: return 'ragged shower rain'; // 09d // Rain
    case 600: return 'light snow'; // 13d // Snow
    case 601: return 'Snow'; // 13d // Snow
    case 602: return 'Heavy snow'; // 13d // Snow
    case 611: return 'Sleet'; // 13d // Snow
    case 612: return 'Light shower sleet'; // 13d // Snow
    case 613: return 'Shower sleet'; // 13d // Snow
    case 615: return 'Light rain and snow'; // 13d // Snow
    case 616: return 'Rain and snow'; // 13d // Snow
    case 620: return 'Light shower snow'; // 13d // Snow
    case 621: return 'Shower snow'; // 13d // Snow
    case 622: return 'Heavy shower snow'; // 13d // Snow
    case 701: return 'mist'; // 50d // Mist
    case 711: return 'Smoke'; // 50d // Smoke
    case 721: return 'Haze'; // 50d // Haze
    case 731: return 'sand/ dust whirls'; // 50d // Dust
    case 741: return 'fog'; // 50d // Fog
    case 751: return 'sand'; // 50d // Sand
    case 761: return 'dust'; // 50d // Dust
    case 762: return 'volcanic ash'; // 50d // Ash
    case 771: return 'squalls'; // 50d // Squall
    case 781: return 'tornado'; // 50d // Tornado
    case 800: return 'clear sky'; // 01d // Clear
    case 801: return 'few clouds: 11-25%'; // 02d // Clouds
    case 802: return 'scattered clouds: 25-50%'; // 03d // Clouds
    case 803: return 'broken clouds: 51-84%'; // 04d // Clouds
    case 804: return 'overcast clouds: 85-100%'; // 04d // Clouds
  }
};

const formatTimestamp = (timestamp, noDateIfToday = true, noDateIfTomorrow = true, noDateIfYesterday = true) => {
  const now = new Date();
  const d = new Date(timestamp * 1000);
  if (noDateIfToday && now.toLocaleDateString() === d.toLocaleDateString())
    return d.toLocaleTimeString('en-GB', { hour12: false });
  const tomorrow = new Date(now);
  tomorrow.setDate(tomorrow.getDate() + 1);
  if (noDateIfTomorrow && tomorrow.toLocaleDateString() === d.toLocaleDateString())
    return d.toLocaleTimeString('en-GB', { hour12: false });
  const yesterday = new Date(now);
  yesterday.setDate(yesterday.getDate() - 1);
  if (noDateIfYesterday && yesterday.toLocaleDateString() === d.toLocaleDateString())
    return d.toLocaleTimeString('en-GB', { hour12: false });
  return `
    ${now.toLocaleDateString() === d.toLocaleDateString() ? 'Today' : d.toLocaleDateString()}
    ${d.toLocaleTimeString('en-GB', { hour12: false })}`;
};

const relativeTimestamp = (timestamp, id) => {
  if (!window.relativeTimestampTimeout) window.relativeTimestampTimeout = [];
  window.relativeTimestampTimeout[id] && window.clearTimeout(window.relativeTimestampTimeout[id]);
  window.relativeTimestampTimeout[id] = window.setTimeout(() => relativeTimestamp(timestamp, id), 1000);
  const now = new Date();
  const d = new Date(timestamp * 1000);
  const secondDiff = Math.round((d.getTime() - now.getTime()) / 1000);
  const element = document.getElementById(id);
  const content = `
  <span>
    <i class="fas fa-${secondDiff >= 0 ? 'chevron-right' : 'chevron-left'}"></i>
    <i class="fas fa-sync-alt"></i>
  </span>
  ${Math.abs(secondDiff)}s`;
  if (element) element.innerHTML = content;
  return content;
};

const degreesToOrientation = (degrees) => {
  if (degrees < 22.5 || degrees >= 337.5) return 'N';
  if (degrees < 67.5) return 'NE';
  if (degrees < 112.5) return 'E';
  if (degrees < 157.5) return 'SE';
  if (degrees < 202.5) return 'S';
  if (degrees < 247.5) return 'SW';
  if (degrees < 292.5) return 'W';
  return 'NW';
};

const uvIndexToColor = (uvi) => {
  if (uvi < 3) return 'green';
  if (uvi < 6) return 'yellow';
  if (uvi < 8) return 'orange';
  if (uvi < 11) return 'red';
  return 'purple';
  // UV index (uvi):
  // 0, 1, 2 -	Low - No protection needed
  // 3, 4, 5 - Moderate - Requires protection
  // 6, 7	- High - Requires protection
  // 8, 9, 10 - Very high - Requires extra protection
  // 11+ - Extreme - Stay inside
};

const uvIndexToText = (uvi, withIndex = true) => {
  if (uvi < 3) return `Low, it's safe${withIndex ? `: ${uvi}` : ''}`;
  if (uvi < 6) return `moderate, protection required${withIndex ? `: ${uvi}` : ''}`;
  if (uvi < 8) return `Hight, protection required${withIndex ? `: ${uvi}` : ''}`;
  if (uvi < 11) return `Very high, protection required${withIndex ? `: ${uvi}` : ''}`;
  return `Extreme, stay inside${withIndex ? `: ${uvi}` : ''}`;
};

const formatMeters = (distance) => (distance > 1000 ? `${Math.round(distance / 100) / 10}km` : `${distance}m`);

const formatPressure = (pressure, indicator = true) =>
  `${pressure} hPa${
    indicator
      ? pressure < 1013
        ? '&nbsp;<i class="fas fa-long-arrow-alt-down"></i>'
        : '&nbsp;<i class="fas fa-long-arrow-alt-up"></i>'
      : ''
  }`;

const temperatureToFontAwesome = (temperature) => {
  if (temperature >= 30) return '<i class="fas fa-thermometer-full"></i>';
  if (temperature >= 20) return '<i class="fas fa-thermometer-three-quarters"></i>';
  if (temperature >= 10) return '<i class="fas fa-thermometer-half"></i>';
  if (temperature >= 0) return '<i class="fas fa-thermometer-quarter"></i>';
  return '<i class="fas fa-thermometer-empty"></i>';
};

const iconToFontAwesome = (icon) => {
  // prettier-ignore
  switch (icon) {
    case '01d': return '<i class="main fas fa-sun"></i>'; // clear-sky // day
    case '02d': return '<i class="main fas fa-cloud-sun"></i>'; // few-clouds (from 1/8 to 2/8)
    case '03d': return '<i class="main fas fa-cloud"></i>'; // scattered-clouds (from 3/8 to 4/8)
    case '04d': return '<i class="main fas fa-cloud"></i>'; // broken-clouds (from 5/8 to 7/8) + overcast-clouds (8/8)
    case '09d': return '<i class="main fas fa-cloud-showers-heavy"></i>'; // shower-rain
    case '10d': return '<i class="main fas fa-cloud-sun-rain"></i>'; // rain
    case '11d': return '<i class="main fas fa-bolt"></i>'; // thunderstorm
    case '13d': return '<i class="main fas fa-snowflake"></i>'; // snow
    case '50d': return '<i class="main fas fa-smog"></i>'; // mist
    case '01n': return '<i class="main fas fa-moon"></i>'; // night
    case '02n': return '<i class="main fas fa-cloud-moon"></i>';
    case '03n': return '<i class="main fas fa-cloud"></i>';
    case '04n': return '<i class="main fas fa-cloud"></i>';
    case '09n': return '<i class="main fas fa-cloud-showers-heavy"></i>';
    case '10n': return '<i class="main fas fa-cloud-moon-rain"></i>';
    case '11n': return '<i class="main fas fa-bolt"></i>';
    case '13n': return '<i class="main fas fa-snowflake"></i>';
    case '50n': return '<i class="main fas fa-smog"></i>';
  }
};

const getDateTime = (id = 'clock', timeout = 1000) => {
  window.dateTimeTimeout && window.clearTimeout(window.dateTimeTimeout);
  window.dateTimeTimeout = window.setTimeout(getDateTime, timeout);
  const d = new Date();
  const element = document.getElementById(id);
  const date = `${d.getDate().toString().padStart(2, '0')}/${(d.getMonth() + 1)
    .toString()
    .padStart(2, '0')}/${d.getFullYear()}`; // toLocaleDateString timezone not working
  const time = d.toLocaleTimeString('en-GB', { hour12: false });
  if (element) element.innerHTML = `<span><i class="fas fa-clock"></i></span> ${date} ${time}`;
};

const timestampToDayOfWeekLetters = (timestamp) => {
  const d = new Date(timestamp * 1000);
  const options = { weekday: 'long' };
  const dayOfWeek = new Intl.DateTimeFormat('en-US', options).format(d);
  return dayOfWeek.substring(0, 2);
};

const timestampToHour = (timestamp) => {
  const d = new Date(timestamp * 1000);
  return d.toLocaleTimeString('en-GB', { hour12: false }).split(':')[0];
};

const formatWindSpeed = (windSpeed) => {
  if (windSpeed < 1000) {
    windSpeed = Math.round(windSpeed);
    const divClass = windSpeed > 9 ? ' class="double"' : '';
    // const fontSize = windSpeed > 99 ? 27 : windSpeed > 9 ? 41 : 0;
    // const style = fontSize ? ` style="font-size: ${fontSize}px;"` : '';
    // return `<span${style}>${windSpeed}</span><small> m/s</small>`;
    return `<div${divClass}>${windSpeed}<small> m/s</small></div>`;
  }
  windSpeed = Math.round(windSpeed / 1000);
  const divClass = windSpeed > 9 ? ' class="double"' : '';
  // const fontSize = windSpeed > 99 ? 27 : windSpeed > 9 ? 30 : 0;
  // const style = fontSize ? ` style="font-size: ${fontSize}px;"` : '';
  // return `<span${style}>${windSpeed}</span><small> km/s</small>`;
  return `<div${divClass}>${windSpeed}<small> km/s</small></div>`;
};

const formatPercent = (value) => `<div${value === 100 ? ' class="percent-max"' : ''}>${value}<small>%</small></div>`;

const logNumberOfRequest = (prefix = false) => {
  const now = new Date();
  const currentYear = now.getFullYear();
  const currentMonth = `0${now.getMonth() + 1}`.slice(-2);
  const currentDay = `0${now.getDate()}`.slice(-2);
  const today = `${currentYear}${currentMonth}${currentDay}`;
  const key = `nb-request-${today}${prefix ? `-${prefix}` : ''}`;
  const previousNbAPIquery = localStorage.getItem(key);
  const nbAPIquery = (previousNbAPIquery && parseInt(previousNbAPIquery, 10) + 1) || 1;
  localStorage.setItem(key, nbAPIquery);
};

const oneCallToCurrentWeatherHTMLFlex = (
  {
    clouds,
    dew_point,
    dt,
    temp,
    feels_like,
    humidity,
    pressure,
    sunrise,
    sunset,
    uvi,
    visibility,
    wind_deg,
    wind_speed,
    weather: [{ id, main, description, icon }],
  },
  { nextUpdate, withMainInfo = true },
) => {
  visibility = parseInt(visibility, 10);
  // description = weatherIdToDescription(230); // longuest description
  description = description.charAt(0).toUpperCase() + description.slice(1);
  if (description.toLowerCase().indexOf(main.toLowerCase()) !== -1) main = description;
  // prettier-ignore
  return `
    <section id="weather-current-flex">
      <div class="row current-time"">
        <div id="last-update">${relativeTimestamp(dt, 'last-update')}</div>
        <div id="clock"></div>
        ${nextUpdate ? `<div id="next-update">${relativeTimestamp(nextUpdate, 'next-update')}</div>` : null}
      </div>

      ${withMainInfo ? `
      <div class="row main-info">
        <div class="trend">${iconToFontAwesome(icon)}</div>
        <div class="temp">${temperatureToFontAwesome(temp)}</div>
        <div class="wind">${windCompass(wind_deg, 150)}</div>
      </div>` : ''}

      <div class="row center">
        <span>${withMainInfo ? '<i class="fas fa-chart-line"></i>' : iconToFontAwesome(icon)}</span>
        <!--${weatherIdToDescription(id)}-->${main}
      </div>
      ${main !== description
        ? `<div class="row center"><span><i class="fas fa-ellipsis-h"></i></span>${description}</div>`
        : ''}
      <div class="row center"><span><i class="fas fa-sun"></i></span>${uvIndexToText(uvi)}</div>

      <div class="row">
        <div><span><i class="fas fa-temperature-high"></i></span>${Math.round(temp)}°C</div>
        <div><span><i class="fas fa-cloud"></i></span>${clouds}%</div>
        <div><span><i class="fas fa-eye"></i></span>${formatMeters(visibility)}</div>
      </div>

      <div class="row">
        <div><span><i class="fas fa-allergies"></i></span>${Math.round(feels_like)}°C</div>
        <div><span><i class="fas fa-tint"></i></span>${humidity}%</div>
        <div><span><i class="fas fa-tint"></i><sup><i class="fas fa-dot-circle"></i></span></sup>
          ${Math.round(dew_point)}°C</div>
      </div>

      <div class="row">
        <div><span><i class="fas fa-weight-hanging"></i></span>${formatPressure(pressure)}</div>
        <div><span><i class="fas fa-wind"></i></span>${wind_speed} m/s</div>
      </div>

      <div class="row">
        <div><span><i class="fas fa-sun"></i><i class="fas fa-long-arrow-alt-up"></i></span>
          ${formatTimestamp(sunrise)}</div>
        <div><span><i class="fas fa-sun"></i><i class="fas fa-long-arrow-alt-down"></i></span>
          ${formatTimestamp(sunset)}</div>
      </div>
    </section>`;
};

const oneCallToWeather8DaysForecastHTMLFlex = (heightDays) => {
  // [{
  //   clouds: 57,
  //   dew_point: -8.49,
  //   dt: 1612609200,
  //   feels_like: {
  //     day: -11.65
  //     eve: -14.48
  //     morn: -15.64
  //     night: -13.95
  //   },
  //   humidity: 94,
  //   pop: 0.41,
  //   pressure: 1029,
  //   sunrise: 1612593980,
  //   sunset: 1612624641,
  //   temp: {
  //     day: -5.87
  //     eve: -9.23
  //     max: -5.84
  //     min: -10.61
  //     morn: -10.5
  //     night: -8.55
  //   },
  //   uvi: 0.42,
  //   weather: [{id: 803, main: "Clouds", description: "broken clouds", icon: "04d"}],
  //   wind_deg: 331,
  //   wind_speed: 4.29
  // }]

  return `
    <div class="column-9">
      <div>&nbsp;</div>
      <div><i class="fas fa-chart-line"></i></div>
      <div><i class="fas fa-thermometer-half"></i><i class="fas fa-long-arrow-alt-up"></i></div>
      <div><i class="fas fa-thermometer-half"></i><i class="fas fa-long-arrow-alt-down"></i></div>
      <div><i class="fas fa-allergies"></i></i><i class="fas fa-long-arrow-alt-up"></i></div>
      <div><i class="fas fa-allergies"></i></i><i class="fas fa-long-arrow-alt-down"></i></div>
      <div><i class="fas fa-sun"></i></div>
      <div><i class="fas fa-wind"></i></div>
      <div><i class="fas fa-cloud"></i></div>
      <div><i class="fas fa-tint"></i></div>
    </div>
    ${heightDays
      .map(
        ({
          dt,
          weather: [{ icon }],
          temp: { min, max },
          feels_like: { day, eve, morn, night },
          uvi,
          wind_speed,
          clouds,
          humidity,
        }) => `
    <div class="column-9">
      <div>${timestampToDayOfWeekLetters(dt)}</div>
      <div>${iconToFontAwesome(icon)}</div>
      <div>${Math.round(max)}</div>
      <div>${Math.round(min)}</div>
      <div>${Math.round(Math.max(day, eve, morn, night))}</div>
      <div>${Math.round(Math.min(day, eve, morn, night))}</div>
      <div>${Math.round(uvi * 10) / 10}</div>
      ${formatWindSpeed(wind_speed)}
      ${formatPercent(clouds)}
      ${formatPercent(humidity)}
    </div>`,
      )
      .join('')}`;
};

const oneCallToWeather8HoursForecastHTMLFlex = (heightDays) => {
  // clouds: 90
  // dew_point: -7.56
  // dt: 1612645200
  // feels_like: -11.27
  // humidity: 86
  // pop: 0.14
  // pressure: 1026
  // temp: -5.82
  // uvi: 0
  // visibility: 7607
  // weather: [{id: 804, main: "Clouds", description: "overcast clouds", icon: "04n"}]
  // wind_deg: 320
  // wind_speed: 3.68

  return `
    <div class="column-9">
      <div>&nbsp;</div>
      <div><i class="fas fa-chart-line"></i></div>
      <div><i class="fas fa-thermometer-half"></i></div>
      <div><i class="fas fa-allergies"></i></div>
      <div><i class="fas fa-sun"></i></div>
      <div><i class="fas fa-wind"></i></div>
      <div><i class="fas fa-cloud"></i></div>
      <div><i class="fas fa-tint"></i></div>
    </div>
    ${heightDays
      .map(({ dt, weather: [{ icon }], temp, feels_like, uvi, wind_speed, clouds, humidity }, i) =>
        i != 0 && i < 9
          ? `
    <div class="column-9">
      <div>${timestampToHour(dt)}h</div>
      <div>${iconToFontAwesome(icon)}</div>
      <div>${Math.round(temp)}</div>
      <div>${Math.round(feels_like)}</div>
      <div>${Math.round(uvi * 10) / 10}</div>
      ${formatWindSpeed(wind_speed)}
      ${formatPercent(clouds)}
      ${formatPercent(humidity)}
    </div>`
          : null,
      )
      .join('')}`;
};

const oneCallToAlertsHTMLFlex = (alerts) => {
  /* alerts = [
    {
      sender_name: 'NWS Tulsa (Eastern Oklahoma)',
      event: 'Heat Advisory',
      start: 1597341600,
      end: 1597366800,
      description:
        '...HEAT ADVISORY REMAINS IN EFFECT FROM 1 PM THIS AFTERNOON TO\n8 PM CDT THIS EVENING...\n* WHAT...Heat index values of 105 to 109 degrees expected.\n* WHERE...Creek, Okfuskee, Okmulgee, McIntosh, Pittsburg,\nLatimer, Pushmataha, and Choctaw Counties.\n* WHEN...From 1 PM to 8 PM CDT Thursday.\n* IMPACTS...The combination of hot temperatures and high\nhumidity will combine to create a dangerous situation in which\nheat illnesses are possible.',
    },
    {
      sender_name: 'NWS Tulsa (Eastern Oklahoma)',
      event: 'Heat Advisory 2',
      start: 1597341600,
      end: 1597366800,
      description:
        '...HEAT ADVISORY REMAINS IN EFFECT FROM 1 PM THIS AFTERNOON TO\n8 PM CDT THIS EVENING...\n* WHAT...Heat index values of 105 to 109 degrees expected.\n* WHERE...Creek, Okfuskee, Okmulgee, McIntosh, Pittsburg,\nLatimer, Pushmataha, and Choctaw Counties.\n* WHEN...From 1 PM to 8 PM CDT Thursday.\n* IMPACTS...The combination of hot temperatures and high\nhumidity will combine to create a dangerous situation in which\nheat illnesses are possible.',
    },
  ]; */

  if (!alerts || !alerts.length) return ``;

  return alerts
    .map(
      ({ event, description, start, end, sender_name }) => `
        <div class="block">
          <h1>${event}</h1>
          <div>${description.replaceAll('\n', '<br>')}</div>
          <div>From ${formatTimestamp(start, true, false, false)} to ${formatTimestamp(end, true, false, false)}</div>
          <div>${sender_name}</div>
        </div>`,
    )
    .join('');
};

const setElementContent = (id, callback) => {
  const element = document.getElementById(id);
  if (!element || typeof callback !== 'function') return;
  element.innerHTML = callback();
};

// 1000 free calls per day for one call API (we use 849 calls)
const oneCallRefreshIntervalDuringNight = 900000; // From 10pm to 7am we update every 7m => 77 calls
const oneCallRefreshIntervalDuringDay = 70000; // From 7am to 10pm we update every 70 seconds => 772 calls

const getOneCallRefreshNextInterval = () => {
  const timestamp = Date.now();
  const d = new Date(timestamp);
  const h = parseInt(d.getHours(), 10);
  const nextOneCallRefreshInterval =
    h >= 7 && h < 22 ? oneCallRefreshIntervalDuringDay : oneCallRefreshIntervalDuringNight;
  const nextOneCallRefreshTimestamp = (timestamp + nextOneCallRefreshInterval) / 1000;
  return { nextOneCallRefreshInterval, nextOneCallRefreshTimestamp };
};

let lastCallOK = true;
let initOneCallUpdateTimeout;
const initOneCallUpdate = async () => {
  const { nextOneCallRefreshInterval, nextOneCallRefreshTimestamp } = getOneCallRefreshNextInterval();
  initOneCallUpdateTimeout = window.setTimeout(initOneCallUpdate, nextOneCallRefreshInterval);
  const oneCallForecast = await getAllByLonLatWithOneCall({ lon: '18.0649', lat: '59.3326' });
  const {
    ok,
    status,
    statusText,
    result: { current, daily, hourly, alerts, cod, message },
  } = oneCallForecast;
  if (!ok || (cod && cod !== 200)) {
    window.clearTimeout(initOneCallUpdateTimeout);
    const now = new Date();
    const nowTimestamp = now.getTime();
    // we plan the next call for the next day or for the next hour
    if (lastCallOK) {
      now.setDate(now.getDate() + 1);
      now.setHours(0);
      now.setMinutes(0);
      now.setSeconds(0);
      now.setMilliseconds(0);
    } else {
      now.setHours(now.getHours() + 1);
    }
    initOneCallUpdateTimeout = window.setTimeout(initOneCallUpdate, now.getTime() - nowTimestamp);
    console.log('Error', cod, message, oneCallForecast);
    document.getElementById('weather-current').innerHTML = `
      <b>Error${cod !== status ? ` status ${status}` : ''} code ${cod}:</b> ${statusText}<br>${message}`;
    document.getElementById('weather-hourly-forecast').innerHTML = `
      <div class="message center">
        Next update: ${now.toLocaleDateString('en-GB', { hour12: false })}
        at ${now.toLocaleTimeString('en-GB', { hour12: false })}
      </div>`;
    document.getElementById('weather-daily-forecast').innerHTML = '';
    lastCallOK = false;
    return;
  }
  lastCallOK = true;

  setElementContent('weather-current', () =>
    oneCallToCurrentWeatherHTMLFlex(current, {
      nextUpdate: nextOneCallRefreshTimestamp,
      withMainInfo: false,
    }),
  );
  setElementContent('weather-hourly-forecast', () => oneCallToWeather8HoursForecastHTMLFlex(hourly));
  setElementContent('weather-daily-forecast', () => oneCallToWeather8DaysForecastHTMLFlex(daily));
  setElementContent('weather-alerts', () => oneCallToAlertsHTMLFlex(alerts));

  getDateTime();
};

window.addEventListener('load', initOneCallUpdate);
